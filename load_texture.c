/*
 *  Lightblaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* 
 *  As stated above this program is distributed under the terms of the
 *  GNU General Public License, However relicensing will be considered on a 
 *  case by case basis. Please feel free to contact the copyright holders 
 *  about this
 */

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "load_texture.h"

int load_texture( char *filename ){
  SDL_Surface *surf = IMG_Load( filename );
  GLuint handle;
  
  printf( "Loading Image:%s\n",filename );

  if( NULL == surf ){
    printf( "error loading file: %s Into SDL surface\n", filename );
    exit( 0 );
  }
  glGenTextures( 1, &handle );
  glBindTexture( GL_TEXTURE_2D, handle );

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  SDL_LockSurface( surf );
  if( surf->flags & SDL_SRCALPHA ){
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surf->w, surf->h,
                                0, GL_RGBA, GL_UNSIGNED_BYTE,
                                surf->pixels );
  } else {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, surf->w, surf->h,
                                0, GL_RGB, GL_UNSIGNED_BYTE,
                                surf->pixels );
  }
  SDL_UnlockSurface( surf );
  return handle;
}
