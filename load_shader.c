/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */



#ifdef _WIN32
#include <windows.h>
#endif

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#include <stdio.h>
#include <stdlib.h>


#include "load_shader.h"

char *load_text_file( const char * filename ){
  char *text;
  size_t fsize;
  FILE *file = fopen( filename, "r" );
  if( file == NULL ){
    printf( "Failed to open file:%s\n", filename );
    exit( 0 );
  }
  fseek( file, 0, SEEK_END );
  fsize = ftell( file );
  rewind( file );
  text = malloc( (fsize+1) * sizeof(char) );
  fread( text, sizeof(char), fsize, file );
  text[fsize] = '\0';
  rewind( file );
  fclose( file );
  return text;
}

void print_shader_log( GLuint obj )
{
  int infologLength = 0;
  int charsWritten  = 0;
  char *infoLog;

  glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

  if (infologLength > 0)
  {
    infoLog = (char *)malloc(infologLength);
    glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
    printf("%s\n",infoLog);
    free(infoLog);
  }
}

void print_program_log(GLuint obj)
{
  int infologLength = 0;
  int charsWritten  = 0;
  char *infoLog;

  glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

  if (infologLength > 0)
  {
    infoLog = (char *)malloc(infologLength);
    glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
    printf("%s\n",infoLog);
    free(infoLog);
  }
}

unsigned int load_shader( const char *fragment_file, const char *vertex_file ){

  GLuint v,f,f2,p;
  char *vs,*fs;
  const char * vv;
  const char * ff;

  if( fragment_file != NULL ){
    printf( "Creating Shader\n"
            "  With fragment shader: %s\n"
            "  And vertex shader :%s\n",
            fragment_file, vertex_file );
  } else {
    printf( "Creating Shader\n"
            "  With vertex shader :%s\n"
            " And no fragment shader!\n",
            vertex_file );
  }

  v = glCreateShader(GL_VERTEX_SHADER);

  if( fragment_file != NULL ){
    f = glCreateShader(GL_FRAGMENT_SHADER);
    f2 = glCreateShader(GL_FRAGMENT_SHADER);
  }
  vs = load_text_file( vertex_file );
  
  if( fragment_file != NULL ){
    fs = load_text_file( fragment_file );
  }
  vv = vs;
  ff = fs;

  glShaderSource(v, 1, &vv,NULL);

  if( fragment_file != NULL ){
    glShaderSource(f, 1, &ff,NULL);
  }
  free(vs);

  if( fragment_file != NULL ){
    free(fs);
  }
  glCompileShader(v);

  if( fragment_file != NULL ){
    glCompileShader(f);
  }
  print_shader_log(v);

  if( fragment_file != NULL ){
    print_shader_log(f);
    print_shader_log(f2);
  }
  p = glCreateProgram();
  glAttachShader(p,v);

  if( fragment_file != NULL ){
    glAttachShader(p,f);
  }
  glLinkProgram(p);
  print_program_log(p);

  glUseProgram(p);

  return p;
}

