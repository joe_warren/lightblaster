/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include "cart_coords.h"

#define SHADOW_MAP_SIZE 512
#define SHADOW_NEAR 0.01
#define SHADOW_FAR 50.0

#define SUBIMAGE_SIZE 1024

#define NUM_LIGHTS 6

struct lightPoint{
  cartCoords initial_position;

  cartCoords position;
  cartCoords velocity;

  float final_matrix[6][16];
  float view_matrix[16];
  float face_matrix[6][16];
  float projection_matrix[16];

  int active;

  unsigned int tex_depth[6];
  unsigned int depth_cube;
  unsigned int framebuffer[6];
  unsigned int subimage_framebuffer;
  unsigned int tex_subimage;
};


struct gameData{

    pthread_mutex_t lock;

    void *icon;

    float x;
    float y;
    float z; 

    unsigned int room;
    
    unsigned int depth_shader;

    unsigned int shader;
    unsigned int lightposUniformLoc;

    unsigned int sidematUniformLoc[6];
    unsigned int shadowmapUniformLoc[6];
    unsigned int lightviewmatUniformLoc;
    unsigned int lightprojmatUniformLoc;

    unsigned int light_tex_shader;
    unsigned int lightTextureLoc;

    unsigned int final_image_shader;
    unsigned int subImageTextureLoc;

    int currentLight;

    struct lightPoint light[NUM_LIGHTS];

    int forward;
    int right;

    float angle;
    float azimuth;

    void *soundtrack_audio;
    void *explosion_audio;

    int light_tex;
};

struct gameData * load_data ( char * file );

void config_error( char * error );
 

