uniform vec3 lightpos;

uniform mat4 lightviewmat;
uniform mat4 lightprojmat;

varying vec4 lightrel; 
varying vec3 normal;
varying vec4 pos; 

void main()
{
    normal = gl_Normal.xyz;
	
    lightrel = vec4( lightpos.xyz - gl_Vertex.xyz/gl_Vertex.w, 1.0 );
 
    pos = gl_Vertex; 
    vec4 mViewCoord = gl_ModelViewMatrix * gl_Vertex;
    gl_Position = gl_ProjectionMatrix * mViewCoord;
}
