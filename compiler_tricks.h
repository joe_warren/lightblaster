/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* 
 *  As stated above this program is distributed under the terms of the
 *  GNU General Public License, However relicensing will be considered on a 
 *  case by case basis. Please feel free to contact the copyright holders 
 *  about this
 */

#ifndef COMPILER_TRICKS_H
#define COMPILER_TRICKS_H


/* An Unused Macro 
 *  can create a function with an unused parameter
 *  for instance;
 *  int return_r( int r, int UNUSED(a) ){ return r; }
 *  this allows similar funtions to have identical parameter lists 
 *  even if one function does not make use of all of the same parameters
 *  it also allows creating functions for callbacks which pass uneeded values
 */
#ifdef __GNUC__
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif

/*
 * This is a set of macro definitions, 
 * These are used to create the Preprocessor "metaprogramming" list types
 * Their use is discouraged, unless it's for a good reason
 */
#define _CAT2(x,y) x##y

/* Concatenate 2 tokens x and y */
#define CAT2(x,y) _CAT2(x,y)
/* Concatenate 3 tokens x, y and z */
#define CAT3(x,y,z) CAT2(x,CAT2(y,z))

/* Join 2 tokens x and y with '_' = x_y */
#define JOIN2(x,y) CAT3(x,_,y)
/* Join 3 tokens x, y and z with '_' = x_y_z */
#define JOIN3(x,y,z) JOIN2(x,JOIN2(y,z)) 
/* Compute the memory footprint of n T's */
#define SPAN(n,T)   ((n) * sizeof(T))

#endif /* COMPILER_TRICKS_H */


