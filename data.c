/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>

#include "data.h"
#include "load_obj.h"
#include "load_shader.h"
#include "load_texture.h"

void config_error( char * error ){
  printf( "%s",error );
  exit( -1 );
}

void setup_light( struct lightPoint * light, int index ){
  int i;
  GLuint rboDepthStencil;

  light->position.x = 0.0;
  light->position.y = 0.5;
  light->position.z = 0.0;

  light->velocity.x = 0.0;
  light->velocity.y = 0.0;
  light->velocity.z = 0.0;

  light->active = 0;

  for( i = 0; i < 6; i++ ){
    glActiveTexture(GL_TEXTURE0 + i );

    glGenTextures(1, &(light->tex_depth[i]) );
    glBindTexture(GL_TEXTURE_2D, light->tex_depth[i]);

    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		  SHADOW_MAP_SIZE, SHADOW_MAP_SIZE,
		  0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL
    );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

  }
  glBindTexture(GL_TEXTURE_2D, 0);

  glGenFramebuffers(6, light->framebuffer );

  for( i = 0; i< 6; i++ ){
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE_2D, light->tex_depth[i]);

    glBindFramebuffer( GL_FRAMEBUFFER, light->framebuffer[i] );

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_TEXTURE_2D, light->tex_depth[i], 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
  }

  glGenFramebuffers(1, &(light->subimage_framebuffer) );
  glBindFramebuffer(GL_FRAMEBUFFER, light->subimage_framebuffer);

  glGenRenderbuffers(1, &rboDepthStencil);
  glBindRenderbuffer(GL_RENDERBUFFER, rboDepthStencil);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8,
		                SUBIMAGE_SIZE, SUBIMAGE_SIZE);
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rboDepthStencil
  );
  glBindRenderbuffer(GL_RENDERBUFFER, 0);

  glActiveTexture(GL_TEXTURE0+7 + index);

  glGenTextures(1, &(light->tex_subimage) );
  glBindTexture(GL_TEXTURE_2D, light->tex_subimage);
	
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,
                SUBIMAGE_SIZE, SUBIMAGE_SIZE,
                0, GL_RGB, GL_UNSIGNED_BYTE, NULL
    );

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glBindFramebuffer(GL_FRAMEBUFFER, light->subimage_framebuffer);

  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, light->tex_subimage, 0
  );

  glEnable(GL_DEPTH_TEST);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  /* create light projection matrix */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(90.0, 1.0, SHADOW_NEAR, SHADOW_FAR);
  glGetFloatv(GL_PROJECTION_MATRIX, light->projection_matrix);
  glMatrixMode(GL_MODELVIEW);

  /* create light face matrices */
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0,  1.0, 0.0, 0.0,  0.0,-1.0, 0.0); /* +X */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[0]);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0, -1.0, 0.0, 0.0,  0.0,-1.0, 0.0); /* -X */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[1]);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0,  0.0, 1.0, 0.0,  0.0, 0.0, 1.0); /* +Y */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[2]);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0,  0.0,-1.0, 0.0,  0.0, 0.0,-1.0); /* -Y */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[3]);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0,  0.0, 0.0, 1.0,  0.0,-1.0, 0.0); /* +Z */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[4]);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0,  0.0, 0.0,-1.0,  0.0,-1.0, 0.0); /* -Z */
  glGetFloatv(GL_MODELVIEW_MATRIX, light->face_matrix[5]);
}

struct gameData * load_data ( char * file ){
  int i;
  struct gameData * data = malloc( sizeof( struct gameData ) );
 
  data->x = 0.0;
  data->y = 0.5;
  data->z = 0.0;

  data->currentLight = 0;

  for( i = 0; i< NUM_LIGHTS; i++ ){
    setup_light( &(data->light[i]), i );
  }

  data->room = load_obj_data( file );
  data->shader = load_shader( "shader.frag", "shader.vert" );
  data->lightposUniformLoc = glGetUniformLocation( data->shader, "lightpos" );

  data->shadowmapUniformLoc[0] =
    glGetUniformLocation(data->shader, "shadowmapA");
  data->shadowmapUniformLoc[1] =
    glGetUniformLocation(data->shader, "shadowmapB");
  data->shadowmapUniformLoc[2] =
    glGetUniformLocation(data->shader, "shadowmapC");
  data->shadowmapUniformLoc[3] =
    glGetUniformLocation(data->shader, "shadowmapD");

  data->shadowmapUniformLoc[4] =
    glGetUniformLocation(data->shader, "shadowmapE");
  data->shadowmapUniformLoc[5] =
    glGetUniformLocation(data->shader, "shadowmapF");


  data->sidematUniformLoc[0] =
    glGetUniformLocation(data->shader, "sidematA");
  data->sidematUniformLoc[1] =
    glGetUniformLocation(data->shader, "sidematB");
  data->sidematUniformLoc[2] =
    glGetUniformLocation(data->shader, "sidematC");
  data->sidematUniformLoc[3] =
    glGetUniformLocation(data->shader, "sidematD");
  data->sidematUniformLoc[4] =
    glGetUniformLocation(data->shader, "sidematE");
  data->sidematUniformLoc[5] =
    glGetUniformLocation(data->shader, "sidematF");

  data->lightviewmatUniformLoc = 
    glGetUniformLocation( data->shader, "lightviewmat" );
  data->lightprojmatUniformLoc =
    glGetUniformLocation(data->shader, "lightprojmat");

  data->depth_shader = load_shader( NULL, "shadow.vert" );

  data->final_image_shader = load_shader( "combine.frag", "combine.vert" );
  data->subImageTextureLoc =
    glGetUniformLocation(data->final_image_shader, "tex");

  data->light_tex_shader = load_shader( "texture.frag", "texture.vert" );
  data->lightTextureLoc =
    glGetUniformLocation(data->final_image_shader, "tex");

  data->forward = 0;
  data->right=0; 
  data->angle = 0.0;
  data->azimuth = 0.0;

  if( (data->explosion_audio = Mix_LoadWAV("explosion.ogg")) == NULL){
    printf("Unable to load audio file: %s\n", Mix_GetError());
    exit(1); 
  } 
  if( (data->soundtrack_audio = Mix_LoadWAV("soundtrack.ogg")) == NULL){
    printf("Unable to load audio file: %s\n", Mix_GetError());
    exit(1); 
  } 

  if( Mix_PlayChannel(-1,data->soundtrack_audio, -1) == -1 ){
    printf("Unable to play soundtrack: %s\n", Mix_GetError());
  }

  glActiveTexture(GL_TEXTURE0);
  data->light_tex = load_texture( "light_tex.png" );  

  pthread_mutex_init ( &(data->lock), NULL );
  
  return data;
}
