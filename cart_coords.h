/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* 
 *  As stated above this program is distributed under the terms of the
 *  GNU General Public License, However relicensing will be considered on a 
 *  case by case basis. Please feel free to contact the copyright holders 
 *  about this
 */



#ifndef CART_COORDS_H
#define CART_COORDS_H

typedef struct {
  float x;
  float y; 
  float z;
} cartCoords;

typedef enum { 
  CART_COORD_AXIS_X, 
  CART_COORD_AXIS_Y,
  CART_COORD_AXIS_Z
} cartCoordAxis;
 
cartCoords add_cart_coords( cartCoords a, cartCoords b );
cartCoords subtract_cart_coords( cartCoords a, cartCoords b);
cartCoords scalar_multiply_cart_coords( float scalar, cartCoords a );
void populate_3_by_3_matrix( float m[3][3], 
                             float a11, float a12, float a13,
                             float a21, float a22, float a23,
                             float a31, float a32, float a33 );
cartCoords matrix_multiply_cart_coords( float m[3][3], cartCoords a );
cartCoords rotate_cart_coords_about_axis( float angle, 
                                          cartCoords a,
                                          cartCoordAxis axis );
cartCoords make_cart_coords_positive( cartCoords a );
cartCoords normalize_cart_coords( cartCoords a );
float length_of_cart_coords( cartCoords a);
float angle_in_x_y_plane_degrees(cartCoords a);
int equal_cart_coords(cartCoords a, cartCoords b);
float get_length(cartCoords a, cartCoords b);
float get_length_in_x_y(cartCoords a, cartCoords b);
cartCoords get_cart_coords_from_polar_coords(float length, float angle);
cartCoords get_heading_from_bearing(float bearing);
float get_bearing_in_degrees(cartCoords a);

#endif /* CART_COORDS_H */
