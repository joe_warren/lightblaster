/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* 
 *  As stated above this program is distributed under the terms of the
 *  GNU General Public License, However relicensing will be considered on a 
 *  case by case basis. Please feel free to contact the copyright holders 
 *  about this
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "load_obj.h"

void color4_to_float4(const struct aiColor4D *c, float f[4])
{
	f[0] = c->r;
	f[1] = c->g;
	f[2] = c->b;
	f[3] = c->a;
}

/****************************************************************************/
void set_float4(float f[4], float a, float b, float c, float d)
{
	f[0] = a;
	f[1] = b;
	f[2] = c;
	f[3] = d;
}

/****************************************************************************/
void apply_material(const struct aiMaterial *mtl)
{
  float c[4];

  /* GLenum fill_mode;*/
  int ret1, ret2;
  struct aiColor4D diffuse;
  struct aiColor4D specular;
  struct aiColor4D ambient;
  struct aiColor4D emission;
  float shininess, strength;
  int two_sided;
  /* int wireframe; */
  unsigned int max;

  set_float4(c, 0.8f, 0.8f, 0.8f, 1.0f);
  if( AI_SUCCESS == aiGetMaterialColor(
                         mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse )){
    color4_to_float4(&diffuse, c);
  }
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c);
        

  set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
  if(AI_SUCCESS == aiGetMaterialColor(
                        mtl, AI_MATKEY_COLOR_SPECULAR, &specular)){
    color4_to_float4(&specular, c);
  }
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);

  set_float4(c, 0.2f, 0.2f, 0.2f, 1.0f);
  if(AI_SUCCESS == aiGetMaterialColor(
                        mtl, AI_MATKEY_COLOR_AMBIENT, &ambient)){
    color4_to_float4(&ambient, c);
  }
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c);

  set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
  if(AI_SUCCESS == aiGetMaterialColor(
                        mtl, AI_MATKEY_COLOR_EMISSIVE, &emission)){
    color4_to_float4(&emission, c);
  }
  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);

  max = 1;
  ret1 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
  if(ret1 == AI_SUCCESS) {
    max = 1;
    ret2 = aiGetMaterialFloatArray(
             mtl, AI_MATKEY_SHININESS_STRENGTH, &strength, &max);
    if(ret2 == AI_SUCCESS){
      glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess * strength/100.0);
    } else {
      glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess/100.0);
    }
  } else {
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);
    set_float4(c, 0.0f, 0.0f, 0.0f, 0.0f);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);
  }

  /* Do not set the polygon mode when assimp says so
   */
  /*
   * max = 1;
   * if(AI_SUCCESS == aiGetMaterialIntegerArray(
   *                      mtl, AI_MATKEY_ENABLE_WIREFRAME, &wireframe, &max)){
   *   fill_mode = wireframe ? GL_LINE : GL_FILL;
   * } else {
   *   fill_mode = GL_FILL;
   * }
   *
   * glPolygonMode(GL_FRONT_AND_BACK, fill_mode);
   */
  max = 1;
  if((AI_SUCCESS == aiGetMaterialIntegerArray(
                         mtl, AI_MATKEY_TWOSIDED, &two_sided, &max)) 
      && two_sided){
    /*glDisable(GL_CULL_FACE);
     */
  } else {
    /* glEnable(GL_CULL_FACE);
     */
  }
}

/****************************************************************************/
void recursive_render (const struct aiScene *sc, const struct aiNode* nd)
{
	unsigned int i;
	unsigned int n = 0, t;
	struct aiMatrix4x4 m = nd->mTransformation;

	/* update transform */
	aiTransposeMatrix4(&m);
	glPushMatrix();
	glMultMatrixf((float*)&m);

	/* draw all meshes assigned to this node */
	for (; n < nd->mNumMeshes; ++n) {
		const struct aiMesh* mesh = sc->mMeshes[nd->mMeshes[n]];

		apply_material(sc->mMaterials[mesh->mMaterialIndex]);

		if(mesh->mNormals == NULL) {
			glDisable(GL_LIGHTING);
		} else {
			glEnable(GL_LIGHTING);
		}

		for (t = 0; t < mesh->mNumFaces; ++t) {
			const struct aiFace* face = &mesh->mFaces[t];
			GLenum face_mode;

			switch(face->mNumIndices) {
				case 1: face_mode = GL_POINTS; break;
				case 2: face_mode = GL_LINES; break;
				case 3: face_mode = GL_TRIANGLES; break;
				default: face_mode = GL_POLYGON; break;
			}

			glBegin(face_mode);

			for(i = 0; i < face->mNumIndices; i++) {
				int index = face->mIndices[i];
				if(mesh->mColors[0] != NULL)
					glColor4fv((GLfloat*)&mesh->mColors[0][index]);
				if(mesh->mNormals != NULL) 
					glNormal3fv(&mesh->mNormals[index].x);
				glVertex3fv(&mesh->mVertices[index].x);
			}

			glEnd();
		}

	}

	/* draw all children */
	for (n = 0; n < nd->mNumChildren; ++n) {
		recursive_render(sc, nd->mChildren[n]);
	}

	glPopMatrix();
}


GLuint load_obj_data( char * filename ){
  const struct aiScene* scene;
  GLuint display_list;
  /* log to the console */
  struct aiLogStream stream = aiGetPredefinedLogStream(
                                  aiDefaultLogStream_STDOUT,NULL);
  aiAttachLogStream(&stream);

  printf( "Loading Object: %s\n", filename );
  if((scene = aiImportFile(filename,
                           0
                           ))==NULL){
    printf( "Failed to load model %s using assimp\n", filename );
    exit( 0 );
  }
 
  display_list = glGenLists(1);
  glNewList(display_list, GL_COMPILE);
    recursive_render(scene, scene->mRootNode);
  glEndList();

  aiReleaseImport(scene); 
  aiDetachAllLogStreams();
  return display_list;
}
