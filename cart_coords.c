/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* 
 *  As stated above this program is distributed under the terms of the
 *  GNU General Public License, However relicensing will be considered on a 
 *  case by case basis. Please feel free to contact the copyright holders 
 *  about this
 */
#include <math.h>
#include "cart_coords.h"


cartCoords add_cart_coords( cartCoords a, cartCoords b ){
  cartCoords r;
  r.x = a.x + b.x;
  r.y = a.y + b.y;
  r.z = a.z + b.z;
  return r;
}

cartCoords subtract_cart_coords( cartCoords a, cartCoords b){
  cartCoords r;
  r.x = a.x - b.x;
  r.y = a.y - b.y;
  r.z = a.z - b.z;
  return r;
}

cartCoords scalar_multiply_cart_coords( float scalar, cartCoords a ){
  a.x *= scalar;
  a.y *= scalar;
  a.z *= scalar;
  return a;
}

void populate_3_by_3_matrix( float m[3][3], 
                             float a11, float a12, float a13,
                             float a21, float a22, float a23,
                             float a31, float a32, float a33 ){
 m[0][0] = a11; m[0][1] = a12; m[0][2] = a13;
 m[1][0] = a21; m[1][1] = a22; m[1][2] = a23;
 m[2][0] = a31; m[2][1] = a32; m[2][2] = a33;
}

cartCoords rotate_cart_coords_about_axis( float angle, 
                                          cartCoords a,
                                          cartCoordAxis axis ){
  float rotmatrix[3][3]; 

  float rads = angle * M_PI /180.0;

  switch( axis ){
    case CART_COORD_AXIS_X:
      populate_3_by_3_matrix( rotmatrix, 
        1,           0,          0, 
        0, cos( rads ), -sin(rads),
        0, sin( rads ),  cos(rads)
      );
    break;
    case CART_COORD_AXIS_Y:
      populate_3_by_3_matrix( rotmatrix, 
        cos( rads ), 0, -sin(rads),
                  0, 1,          0,
        sin( rads ), 0, cos(rads)
      );
      break;
    case CART_COORD_AXIS_Z:
      populate_3_by_3_matrix( rotmatrix, 
        cos( rads ), -sin(rads), 0,
        sin( rads ),  cos(rads), 0,
                  0,          0, 1 
      );
      break;
    default:
      /* default to the identity matrix to avoid breaking loudly */
      populate_3_by_3_matrix( rotmatrix, 
        1, 0, 0,
        0, 1, 0,
        0, 0, 1 
      );
      break;
  }
  return matrix_multiply_cart_coords( rotmatrix, a );
}

cartCoords normalize_cart_coords( cartCoords a ){
  return scalar_multiply_cart_coords( 1.0/length_of_cart_coords( a ), a );

}

cartCoords matrix_multiply_cart_coords( float m[3][3], cartCoords a ){
  cartCoords r;
  r.x = m[0][0]*a.x + m[0][1]*a.y + m[0][2]*a.z; 
  r.y = m[1][0]*a.x + m[1][1]*a.y + m[1][2]*a.z; 
  r.z = m[2][0]*a.x + m[2][1]*a.y + m[2][2]*a.z; 
  return r;
}


float length_of_cart_coords( cartCoords a ){
  return sqrt( a.x*a.x + a.y*a.y + a.z*a.z );
}

cartCoords make_cart_coords_positive( cartCoords a ){
  cartCoords r;
  r.x = fabs( a.x );
  r.y = fabs( a.y );
  r.z = fabs( a.z );
  return r;
}

/* gives the angle between a cart coord projected into the x-y plane 
	 and the x axis in the range -180 to 180 degrees*/
float angle_in_x_y_plane_degrees(cartCoords a){
	float radangle, angle;
	radangle = atan2 (a.y, a.x);
	angle = radangle * 180.0/M_PI;
	return angle;
}

/* returns 1 if two cart coords are equal or 0 if they are not*/
int equal_cart_coords(cartCoords a, cartCoords b) {
	int equal;
	if (a.x == b.x && a.y == b.y && a.z == b.z) {
		equal = 1;
	}
	else { equal = 0; };
	return equal;
}

/* returns the length of the cart coord joining two points*/
float get_length(cartCoords a, cartCoords b) {
	return length_of_cart_coords(
                 subtract_cart_coords(a, b)
               );
}

/* returns length of the cart coord joining two points projected into x-y plane*/
float get_length_in_x_y(cartCoords a, cartCoords b) {
	float length;
	
	a.z = 0.0;
	b.z = 0.0;
	
	length = length_of_cart_coords(
                 subtract_cart_coords(a, b)
               );
	
	return length;
}

/* returns a cart coord in x-y plane from a length and a 
   counterclockwise angle in degrees from x-axis 
*/
cartCoords get_cart_coords_from_polar_coords(float length, float angle) {
	cartCoords c;

	c.x = length * cos (angle * M_PI/180.0);
	c.y = length * sin (angle * M_PI/180.0);
	c.z = 0.0;
  
  return c;
}


/* returns a unit length cart coord in x-y plane from a bearing in degrees 
   taken clockwise from the y-axis (North)
*/
cartCoords get_heading_from_bearing(float bearing) {
	
	float angle = 90.0 - bearing;
	
	cartCoords heading = get_cart_coords_from_polar_coords(1.0, angle);
	  
  return heading;
}

/* gives the bearing of a cart coord in x-y plane in degrees 
  taken clockwise from the y-axis (North)
*/
float get_bearing_in_degrees(cartCoords a){
	float bearing = 90.0 - angle_in_x_y_plane_degrees( a );
	
	if ( bearing < 0.0 ) { bearing += 360.0; }
	
	return bearing;
}
