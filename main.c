/*
 *  LightBlaster
 *
 *  Copyright (C) 2013 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <pthread.h>
#include <stdio.h>

#include "data.h"
#include "logic.h"
#include "render.h"

int main( int argc, char **argv ){
  struct gameData *data;
  pthread_t logic_thread;

  if( argc < 2 ){
     printf( "Missing config file; should use conf.xml or similar\n" );
     return -1;
  }
  initialize_window();

  data = load_data( argv[1] );

  pthread_create( &logic_thread, NULL, logic_main, (void *) data );

  /* render in the main thread, as opengl context has to exist to load data
   *  and this lets us keep this context
   *  to the best of my understanding
   */
  render_main( (void *) data );

  return 0;
}
