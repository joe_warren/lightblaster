/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>

#include <pthread.h>


#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_mixer.h>

#include <math.h>

#include "cart_coords.h"
#include "load_obj.h"
#include "data.h"
#include "compiler_tricks.h"

int window_w, window_h;

void reshape (int w, int h)
{
  window_w = w;
  window_h = h;
  glViewport (0, 0, (GLsizei) w, (GLsizei) h);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (GLfloat) w/(GLfloat) h, SHADOW_NEAR, SHADOW_FAR);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void fullscreen(){
  SDL_Rect **modes = SDL_ListModes( NULL, SDL_HWSURFACE
                                        | SDL_OPENGL
                                        | SDL_FULLSCREEN  );
  if(SDL_SetVideoMode( modes[0]->w, modes[0]->h, 32,
                       SDL_HWSURFACE | SDL_OPENGL | SDL_FULLSCREEN )
                                   == NULL ){
    printf( "failed to create SDL Window\n" );
    exit( -1 );
  }
  reshape( modes[0]->w, modes[0]->h );
}

void exit_fullscreen(){
  if(SDL_SetVideoMode( 640, 480, 32,
                       SDL_HWSURFACE | SDL_OPENGL | SDL_RESIZABLE )
                                   == NULL ){
    printf( "failed to create SDL Window\n" );
    exit( -1 );
  }
  reshape( 640, 480 );
}


void keyboard (int key, int mod, struct gameData *data ){
  int d = 1;
  pthread_mutex_lock( &(data->lock));
  if( mod & KMOD_SHIFT ){
    d = 2;
  }
  switch (key) {
    case SDLK_UP:
    case 'w':
      data->forward = d;
      break;
    case SDLK_DOWN:
    case 's':
      data->forward = -d;
      break;
    case SDLK_LEFT:
    case 'a':
      data->right = -d;
      break;
    case SDLK_RIGHT:
    case 'd':
      data->right = d;
      break;
    case SDLK_RSHIFT:
    case SDLK_LSHIFT:
      data->forward *=2;
      data->right *=2;
      break;
    case SDLK_F11:{
      static int fscreen = 0;
      if( fscreen == 0 ){
        fscreen = 1;
        fullscreen();
      } else {
        fscreen = 0;
        exit_fullscreen();
      }
      break;
    }
    case SDLK_ESCAPE:
      exit( 0 );
      break;
    default:
      break;
  }
  pthread_mutex_unlock( &(data->lock));
}


void keyboard_up ( int key, int UNUSED(mod), struct gameData* data ){

  pthread_mutex_lock( &(data->lock));
  switch (key) {
    case SDLK_UP:
    case 'w':
      data->forward = 0;
      break;
    case SDLK_DOWN:
    case 's':
      data->forward = 0;
      break;
    case SDLK_LEFT:
    case 'a':
      data->right = 0;
      break;
    case SDLK_RIGHT:
    case 'd':
      data->right = 0;
      break;
    case SDLK_RSHIFT:
    case SDLK_LSHIFT:
      data->forward /=2;
      data->right /=2;
    default:
      break;
  }
  pthread_mutex_unlock( &(data->lock));
}


void print_errors(){
  GLenum errCode;
  const GLubyte *errString;

  while ((errCode = glGetError()) != GL_NO_ERROR) {
    errString = gluErrorString(errCode);
    fprintf (stderr, "OpenGL Error: %s\n", errString);
  }
}


void render_cubemap( struct gameData*  data, struct lightPoint * light ){
  int i;
  
  glLoadIdentity();
  glTranslatef(-light->position.x, 
               -light->position.y,
               -light->position.z );
  glGetFloatv(GL_MODELVIEW_MATRIX, light->view_matrix);

  for( i = 0; i < 6; i++ ){

    glBindFramebuffer(GL_FRAMEBUFFER, light->framebuffer[i] );

    if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) 
        != GL_FRAMEBUFFER_COMPLETE  ){
      printf( "incomplete framebuffer %d, %d\n", i,
        glCheckFramebufferStatus( GL_FRAMEBUFFER )
      );
    }

    glViewport(0, 0, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE);
 
    glCullFace(GL_FRONT);

    glClear(GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(light->projection_matrix);

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(light->face_matrix[i]);
    glMultMatrixf(light->view_matrix);
    
    glGetFloatv(GL_MODELVIEW_MATRIX, light->final_matrix[i]);

    glUseProgram(data->depth_shader);
    glCallList( data->room );
  }
}

void setup_light_uniforms( struct gameData* data, struct lightPoint* light ){
  int i;
  for( i = 0; i< 6; i++ ){
    glActiveTexture(GL_TEXTURE0 + i  );
    glBindTexture(GL_TEXTURE_2D, light->tex_depth[i]);
    glUniform1i(data->shadowmapUniformLoc[i], i);
    glUniformMatrix4fv(data->sidematUniformLoc[i], 1, GL_FALSE,
      light->final_matrix[i]);
  }
  glUniform3f(data->lightposUniformLoc, 
    light->position.x,
    light->position.y,
    light->position.z );
  glUniformMatrix4fv(data->lightviewmatUniformLoc,
    1, GL_FALSE, light->view_matrix);
  glUniformMatrix4fv(data->lightprojmatUniformLoc,
    1, GL_FALSE, light->projection_matrix);
}

int get_side_index( cartCoords p ){
  float max = 0.0;
  int i = 0;
  if( p.x > max ){ max = p.x; i=0; }
  if( -p.x > max ){ max = -p.x; i=1; }
  if( p.y > max ){ max = p.y; i=2; }
  if( -p.y > max ){ max = -p.y; i=3; }
  if( p.z > max ){ max = p.z; i=4; }
  if( -p.z > max ){ max = -p.z; i=5; }
  return i;
}

int check_depth_lookup( struct lightPoint* light ){
 int side;
 GLdouble screen_coords[3];
 cartCoords relative_position = subtract_cart_coords(
   light->initial_position,
   light->position
 );
 GLfloat zf;
 GLdouble model[16];
 GLdouble proj[16];
 GLint view[4];

 side = get_side_index( relative_position );
 if( equal_cart_coords( light->position, light->initial_position ) ){
   return 0;
 }
 glBindFramebuffer( GL_FRAMEBUFFER, light->framebuffer[side] );

 glMatrixMode(GL_PROJECTION);
 glLoadMatrixf(light->projection_matrix);

 glMatrixMode(GL_MODELVIEW);
 glLoadMatrixf(light->face_matrix[side]);
 glMultMatrixf(light->view_matrix);

 glGetDoublev(GL_MODELVIEW_MATRIX, model );
 glGetDoublev(GL_PROJECTION_MATRIX, proj );
 glGetIntegerv(GL_VIEWPORT, view );

 if( GLU_FALSE == gluProject(
             light->initial_position.x,
             light->initial_position.y,
             light->initial_position.z,  
             model, proj, view,
             screen_coords, screen_coords+1, screen_coords+2) ){
   printf( "unable to project\n" );
   return 0;
 }
 glReadPixels( screen_coords[0], screen_coords[1], 1, 1,
               GL_DEPTH_COMPONENT, GL_FLOAT, &zf);

 if( zf < screen_coords[2] ){
   printf( "disabling light\n" );
   light->active = 0;
   return 1;
 }
 return 0;
}

void render_light_tex( struct gameData *data, struct lightPoint *light ){
 float half_sz = 0.1;
  glUseProgram( data->light_tex_shader );

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, data->light_tex);
  glUniform1i(data->lightTextureLoc, 0 );

  glEnable( GL_BLEND );
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glDisable( GL_POLYGON_SMOOTH );
  glPushMatrix();
    glTranslated( light->position.x, light->position.y, light->position.z );
    glRotated( -data->angle, 0.0, 1.0, 0.0 );
    glRotated( -data->azimuth, 1.0, 0.0, 0.0 );
    glBegin( GL_QUADS );
      glTexCoord2f( 1.0, 1.0 );
      glVertex3f( half_sz, half_sz, 0.0 );
      glTexCoord2f( 0.0, 1.0 );
      glVertex3f( -half_sz, half_sz, 0.0 );
      glTexCoord2f( 0.0, 0.0 );
      glVertex3f( -half_sz, -half_sz, 0.0);
      glTexCoord2f( 1.0, 0.0 );
      glVertex3f( half_sz, -half_sz, 0.0 ); 
    glEnd();
  glPopMatrix();
  glDisable( GL_POLYGON_SMOOTH );
  glDisable( GL_BLEND );
}


void display( struct gameData *data ){
  int i;

  for( i = 0; i<NUM_LIGHTS; i++ ){

    pthread_mutex_lock( &(data->lock) );
      if( !data->light[i].active ){
        pthread_mutex_unlock( &(data->lock) );
        continue;
      }
      render_cubemap( data, &(data->light[i]) );
      if( check_depth_lookup( &(data->light[i]) ) ){
        cartCoords position;
        float distance;
        int channel;
        position.x = data->x; 
        position.y = data->y; 
        position.z = data->z; 
        distance = length_of_cart_coords( subtract_cart_coords(
          data->light[i].position,
          position
        ) );
        if( (channel = Mix_PlayChannel(-1,data->explosion_audio, 0)) == -1 ){
          printf("Unable to play Explosion sound: %s\n", Mix_GetError());
        } else {
          Mix_Volume( channel, MIX_MAX_VOLUME/fmax(1.0, distance));
        }
      }

    pthread_mutex_unlock( &(data->lock) );

    glBindFramebuffer(GL_FRAMEBUFFER, data->light[i].subimage_framebuffer);
  
    glCullFace(GL_BACK);

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    reshape (window_w, window_h);
    glViewport(0, 0, SUBIMAGE_SIZE, SUBIMAGE_SIZE);

    glUseProgram( data->shader );
    setup_light_uniforms( data, &(data->light[i]) );
      pthread_mutex_lock( &(data->lock) );
        glRotated( data->azimuth, 1.0, 0.0, 0.0 );
        glRotated( data->angle, 0.0, 1.0, 0.0 );
        glTranslated( -data->x, -data->y, -data->z );
      pthread_mutex_unlock( &(data->lock) );
    glCallList( data->room );
    render_light_tex( data, &(data->light[i]) );
  }

  reshape (window_w, window_h);
  glBindFramebuffer(GL_FRAMEBUFFER, 0 );
  glUseProgram( data->final_image_shader );
  glDisable( GL_CULL_FACE );
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glEnable( GL_TEXTURE_2D );
  glBlendFunc(GL_ONE, GL_ONE);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  for( i = 0; i < NUM_LIGHTS; i++ ){
    if( !data->light[i].active ){
      continue;
    }
    glActiveTexture(GL_TEXTURE0+i);
    glBindTexture(GL_TEXTURE_2D, data->light[i].tex_subimage);
    glUniform1i(data->subImageTextureLoc, i);
    glBegin( GL_QUADS );
      glTexCoord2f( 1.0, 1.0 );
      glVertex3f( 1.0, 1.0, 0.0 );
      glTexCoord2f( 0.0, 1.0 );
      glVertex3f( -1.0, 1.0, 0.0 );
      glTexCoord2f( 0.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
      glTexCoord2f( 1.0, 0.0 );
      glVertex3f( 1.0, -1.0, 0.0 );
    glEnd();
  }
  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glEnable( GL_CULL_FACE );
  /* glFlush (); */
  SDL_GL_SwapBuffers();
}

void motion( int x, int y, struct gameData* data ){

  pthread_mutex_lock( &(data->lock) );
    data->angle += 360.0*((((float)x)/(float)window_w)-0.5)
                     * cos( M_PI * data-> azimuth /180.0 ); 
    data->azimuth -= 180.0*(0.5-(((float)y)/(float)window_h)); 
 
    /* range check */
    if(data->angle > 360.0 ) data->angle -=360.0;
    if(data->angle < 0.0 ) data->angle += 360;
    if(data->azimuth > 90.0 ) data-> azimuth = 90.0;
    if(data->azimuth < -90.0 ) data-> azimuth = -90.0;
   
  pthread_mutex_unlock( &(data->lock) );
  SDL_WarpMouse( window_w /2, window_h/2 );
 
}

void create_new_point_light( struct gameData* data ){

 data->light[data->currentLight].active = 1;

 data->light[data->currentLight].position.x = data->x;
 data->light[data->currentLight].position.y = data->y;
 data->light[data->currentLight].position.z = data->z;

 data->light[data->currentLight].initial_position =
   data->light[data->currentLight].position;

 data->light[data->currentLight].velocity.x = 0.0;
 data->light[data->currentLight].velocity.y = 0.0;
 data->light[data->currentLight].velocity.z = -8.0;
 data->light[data->currentLight].velocity =
   rotate_cart_coords_about_axis( -data->azimuth, 
                                  data->light[data->currentLight].velocity,
                                  CART_COORD_AXIS_X );
 data->light[data->currentLight].velocity =
   rotate_cart_coords_about_axis( data->angle, 
                                  data->light[data->currentLight].velocity,
                                  CART_COORD_AXIS_Y );
 data->currentLight++;
 data->currentLight %= NUM_LIGHTS;
}

void click(SDL_Event* e, struct gameData* data ){
  if( e->button.button == SDL_BUTTON_LEFT ){
    if( e->type ==  SDL_MOUSEBUTTONDOWN ){
      create_new_point_light( data );
    }
  }
}


void on_sdl_event(SDL_Event* event, struct gameData* data) {
  switch( event->type ){
    case SDL_QUIT:
      exit( 0 );
      return;
    case SDL_KEYDOWN:
      keyboard( event->key.keysym.sym, event->key.keysym.mod, data );
      return;
    case SDL_KEYUP:
      keyboard_up( event->key.keysym.sym, event->key.keysym.mod, data );
      return;
    case SDL_MOUSEMOTION: 
      motion( event->motion.x, event->motion.y, data );
      return;
    case SDL_MOUSEBUTTONUP:
    case SDL_MOUSEBUTTONDOWN:
      click( event, data);
      return;
    case SDL_VIDEORESIZE: 
      if(( SDL_SetVideoMode(event->resize.w, event->resize.h, 32,
                        SDL_HWSURFACE | SDL_OPENGL
                        | SDL_RESIZABLE ))
                    == NULL) {
        printf( "failed to resize SDL Window\n" );
        exit( -1 );
      }
      reshape( event->resize.w, event->resize.h);
      break;
     
  }
}

void initialize_window(){
  SDL_Surface* window;
  int val; 

  int audio_rate = 22050;
  Uint16 audio_format = AUDIO_U16SYS;
  int audio_channels = 2;
  int audio_buffers = 4096;

  if(SDL_Init(SDL_INIT_EVERYTHING| SDL_INIT_AUDIO) < 0) {
    printf( "failed to initialize SDL\n" );
    exit( -1 );
  }
 
 if(Mix_OpenAudio(
    audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
	fprintf(stderr, "Unable to initialize audio: %s\n", Mix_GetError());
	exit(1);
 }
#if SDL_VERSION_ATLEAST(1,3,0)
  SDL_GL_SetSwapInterval(1);
#else
  /* SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
   */
#endif

  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

  SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );

  SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
  SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES,  4 );
  
  if((window = SDL_SetVideoMode(640, 480, 32,
                        SDL_HWSURFACE | SDL_OPENGL | SDL_RESIZABLE ))
                    == NULL) {
    SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 0 );
    SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 0 );
 
    if((window = SDL_SetVideoMode(640, 480, 32,
                          SDL_HWSURFACE | SDL_OPENGL | SDL_RESIZABLE ))
                      == NULL) {
      printf( "failed to create SDL Window\n" );
      exit( -1 );
    }
  }
  SDL_ShowCursor( SDL_DISABLE ); 
 
  glEnable( GL_CULL_FACE );
  glCullFace( GL_BACK );

  glEnable(GL_MULTISAMPLE_ARB);
  glEnable( GL_POLYGON_SMOOTH );

  window_w = 640; window_h = 480;
  reshape( 640, 480 );

  /* remove the cursor */ 

  /* Initialise */  
  glClearColor (0.0, 0.0, 0.0, 0.0);
  glShadeModel (GL_SMOOTH);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0); /* Ambient Light  */
  glEnable(GL_DEPTH_TEST); 

  glDisable(GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

  glEnable( GL_POINT_SMOOTH );
  glPointSize( 4.0 );

  glEnable(GL_TEXTURE_2D);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);

  printf("GL_RENDERER: %s\n",glGetString(GL_RENDERER));
  printf("GL_VERSION: %s\n", glGetString(GL_VERSION));

  glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &val);
  printf("GL_MAX_TEXTURE_IMAGE_UNITS: %d\n", val);
  glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &val);
  printf("GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS: %d\n", val);
  glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &val);
  printf("GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: %d\n", val);
}

void *render_main( void *arg ){
 
  int t_prev, t_now, frames = 0;
  /* load in game data */
  struct gameData* data = (struct gameData*) arg;

  SDL_Event event;
  t_prev = SDL_GetTicks();
  
  while(1) {
    while(SDL_PollEvent(&event)) {
      on_sdl_event(&event, data);
    }
    if( (t_now = SDL_GetTicks()) > t_prev + 1000 ){
      printf( "%d frames per second\n", frames );
      frames = 0;
      t_prev = t_now;
    }
    display( data );
    frames++;
    print_errors();
  }

  return NULL;
}


