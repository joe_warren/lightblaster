/*
 *  LightBlaster
 *
 *  Copyright (C) 2014 Joseph Warren
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#include <SDL/SDL.h>

#include "logic.h"
#include "data.h"
#include "cart_coords.h"


void *logic_main( void *arg ){
  float x, z, d;
  struct gameData *data = (struct gameData *) arg;
  int t_new, t = SDL_GetTicks();
  int i;
  while( 1 ){
    usleep(25000);
    pthread_mutex_lock( &(data->lock) );

      t_new = SDL_GetTicks();
      d = 0.025 * ((float)(t_new-t)) /25.0 ;
      t = t_new;

      /* motion */
      x = sin( M_PI * data->angle / 180.0 );
      z = -cos( M_PI * data->angle / 180.0 );
      
      data->x += x * d * (float)(data->forward);
      data->z += z * d * (float)(data->forward);

      data->x -= z * d * (float)(data->right);
      data->z += x * d * (float)(data->right);

      /* update light positions */
      for( i = 0; i < NUM_LIGHTS; i++ ){
        data->light[i].position = add_cart_coords(
          data->light[i].position, 
          scalar_multiply_cart_coords( d,
            data->light[i].velocity
          )
        );
      }
      x = cos( M_PI * data->angle / 180.0 );
      z = sin( M_PI * data->angle / 180.0 );

    pthread_mutex_unlock( &(data->lock) );
  }
  return NULL;
}
