uniform sampler2DShadow shadowmapA;
uniform sampler2DShadow shadowmapB;
uniform sampler2DShadow shadowmapC;
uniform sampler2DShadow shadowmapD;
uniform sampler2DShadow shadowmapE;
uniform sampler2DShadow shadowmapF;


uniform mat4 sidematA;
uniform mat4 sidematB;
uniform mat4 sidematC;
uniform mat4 sidematD;
uniform mat4 sidematE;
uniform mat4 sidematF;

uniform mat4 lightprojmat;

uniform mat4 lightviewmat;

varying vec4 lightrel; 
varying vec3 normal;
varying vec4 pos; 

int get_index( vec3 p ){
  float max = 0.0;
  int i = 0;
  if( p.x > max ){ max = p.x; i=0; }
  if( -p.x > max ){ max = -p.x; i=1; }
  if( p.y > max ){ max = p.y; i=2; }
  if( -p.y > max ){ max = -p.y; i=3; }
  if( p.z > max ){ max = p.z; i=4; }
  if( -p.z > max ){ max = -p.z; i=5; }
  return i;
}

mat4 selectsidemat( int i ){
  if( i == 0 ) return sidematA;
  if( i == 1 ) return sidematB;
  if( i == 2 ) return sidematC;
  if( i == 3 ) return sidematD;
  if( i == 4 ) return sidematE;
  if( i == 5 ) return sidematF;
}

vec4 doShadowLookup( vec3 p, int i ){
  if( i == 0 ) return shadow2D(shadowmapA, p );
  if( i == 1 ) return shadow2D(shadowmapB, p );
  if( i == 2 ) return shadow2D(shadowmapC, p );
  if( i == 3 ) return shadow2D(shadowmapD, p );
  if( i == 4 ) return shadow2D(shadowmapE, p );
  if( i == 5 ) return shadow2D(shadowmapF, p );
}

void main()
{
   float lightdist = length( lightrel ); 

   float intensity =  max( 0.0, dot( normalize(normal), normalize(lightrel.xyz) ) );
   intensity *= min( 1.0, 0.5 / (lightdist * lightdist) );

   int i = get_index( -lightrel.xyz );

   vec4 p = lightprojmat * selectsidemat( i ) * pos;

   vec3 pr = p.xyz/p.w; 
   pr= pr.xyz*0.5 +  vec3(0.5,0.5,0.5);

   gl_FragColor = vec4( vec3( 1.0, 1.0 , 1.0 ) * intensity , 1.0) 
     * doShadowLookup( pr, i );
} 
